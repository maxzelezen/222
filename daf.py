def dec(f):
    def wrp(*args):
        print("start")
        y=f(*args)
        print(y)
        print("end")
        return y
    return wrp

@dec
def add(a,b):
    return a+b
x = dec(add)

add(7, 13)
print (add)